# Overview

Squid is a high-performance proxy caching server for web clients, supporting
FTP, gopher, and HTTP data objects.
 
Squid version 3 is a major rewrite of Squid in C++ and introduces a number of
new features including ICAP and ESI support.

http://www.squid-cache.org/

# Usage

## General

This charm provides squid in a reverse proxy setup. 


http://en.wikipedia.org/wiki/Reverse_proxy

The most common scenario is to accelerate a web service:
You run squid on your outside edge, forwarding queries to
one or multiple internal web application servers.

The charm can be deployed in a single or multi-unit setup.

To deploy a single unit:

    juju deploy squid-reverseproxy

To add more units:

    juju add-unit squid-reverseproxy 

Example with apache:

    juju deploy apache2
    juju deploy squid-reverseproxy
    juju add-relation apache2:website-cache squid-reverseproxy:cached-website

This will put squid in front of apache2.

Once deployed, you can ssh into the deployed service:

    juju ssh <unit>

To list running units:

    juju status

To start monitoring Squid using Nagios:

    juju deploy nrpe-external-master
    juju add-relation squid-reverseproxy nrpe-external-master



This charm requires the following relation settings from clients:

    ip: service ip address
    port: service port
    sitenames: space-delimited list of virtual hosts to whitelist

The options that can be configured in config.yaml should be self-explanatory.
If not, please file a bug against this charm.

## HTTPS Reverse Proxying

Assuming you have a squid3 deb compiled with --enable-ssl, you can setup a
single https reverse proxy.

An example of this would be:

    juju set squid-reverseproxy enable_https=true ssl_key="$(base64 < /path/to/cert.key)" ssl_cert="$(base64 < /path/to/cert.crt)"

This should enable https access to the default website.

A current implementation limitation is that it doesn't support multiple https vhosts.

## Monitoring

This charm provides relations that support monitoring via Nagios using 
nrpe_external_master as a subordinate charm.

## Caveats
The example above is just for reference. In order to make it usable, you 
will have to supply a proper virtual host configuration for apache2.

Example of config to deploy squid on CentOS:
squid-reverseproxy:
  services:
    squid-reverseproxy:
      constraints: "tags=squid"
      charm: "local:centos7/squid-reverseproxy"
      options:
        ssl_keyfile: '/etc/squid/ssl/cert.key'
        ssl_certfile: '/etc/squid/ssl/cert.crt'
        ssl_key: 'LS0tLS1CRUdJTiBSU0EgUFJJVkFURSBLRVktLS0tLQ0KTUlJRXBBSUJBQUtDQVFFQTBSSmdmbVRWSnFmcnF4S2lkZjUvWDZwMnVFdTZyb3JBanhkWnVrVmhKeWtORXBUbw0KMkIvWWxLRmlJM2o3VEVaUzVLWGpUVHg4L2E0S2hEa1E0M1ZnM1A5VGU1c1hSV1IzY0MzWUxic3dPU2lWcEhObA0KclpLUkVGK1AwcDJUY2VDMVVCQmI3eFZRbWd3RDNQaHMwQXUzQ3gvUUd0ZDE1OWxPRzJJcGZJLzgzSjNoRmd4YQ0KeEdJcmVscCtHNWwraW9qek1zUndvL3VIdHMxbE5PeHR6RDExbU9yd2poWXcyWTNjM21KbnRzYVlhMXpvdWFxNw0KRjNwUm9lVXhWd2NUMG05cXNJcnF4RE1NWkk3MjRYMFdMekxZV09WWmdNdHZLV1c3LzU4dUNPbTZMd29XakloTA0KRzAwUklTcE96TnlyY1llMHEwNjZmWHZYOG9obE9LR3B0M0QwR1FJREFRQUJBb0lCQVFEUUl4ZGxQVVRtUXUzZg0KVmFIZkRnb0lWMC94a1pLOENPNUNoZUNTREZmNEFHby9RWGU5RVpjd0FLb08zblp2TE9PQk5aVGdZNE9ibVJSbg0KSmRpbVdVaWhPcDhGd3hTYnhMVVRQTkNtVm1CZnh3T1N3RDlOV1VpSm9Ga3lPQ1Y5alZOejdaVllvME9tMktkUA0KNlJHSzNON1JlRTVaS243NjNkOWdhNDBac003dllZRE9XN3FpdnFXNThzK3Yya0pQdnIvWXdtZFdFMW5aYXl5Qg0KSjZOM3NRUHpNM2Y2QnZwdXNCL28yY2xIV3pVV3p1SE5FMEJlSWIzUEc0K3FMQVhmcFN0dXRUdm1xZDJvc0lWRQ0KNUpLdDY2YVdZbDdHOXNzeU4vQytmYnFhdXpNTGgxdFZCTDV0ZUM3WGtZU2l4Q1laTzVxZXVDT1NoVDljNnpMdA0KMzVzV2dBTEJBb0dCQU8zd004N0F3M2Z1dWhKeGwzcnZCbVNRaXhpK2JvYzRXU1pXemZIZVFJM1MySjY4MHAvbQ0KTHVNQjVtS05CbWdiNEpCbDU2K2FrT3ZFT1ZrSlk4SFlURnpWSWZZY0VxUHNUM08rcnh1cjFzNm1SMHdDcDgvSQ0KbjZpWnlqVXdDYlZSWm9BOVFTdzcvV3dMWDZnSXhpaHo0QndNRmdvS05OWGMwTXBveGhGbXZoRGRBb0dCQU9EeA0KTi92Z3ZhSmJiT2RFVkJjUHNnVXQ3bkpmUlNhZEtrQ0x0Rlk3cVFSeWo2V1hVb1dNczA5MUpITW1EMzVTYzhIcw0KUCt4OEtuQWNnaVg0Zm9QemVHZ2lnV0hvMkhZVk9HRUtnR3A0a1dZTjdUNm80czlLYVc3RG5YaHg5TkxSMDdaWA0KY0ZwMUQ5Z0FIWkVqSWlOak9iOTVOcE8vNFpCOSsvanlLQzYySTM1dEFvR0JBSWxtWFRSMmdpbit0SHEwRjhxSQ0KdndOUnFORklIcUY5TzVYMUtJWVVWdWpQS2dJNGZkU1NZdDc2R09VdUZOWUh5eHozaTl3UEFNTDFGRHpEUEZVVA0KSjcydjRpV3h2bDNrdDFqYWMxb2lXREFMT3o2OGhkVnFRREJuWnFwaUxDa0ZjWkI2VHVsUlZ4Mk9UOVhFQTFJVw0KNEFLeVJBUFdNdjZRdmcwMGhiUkV4VnNOQW9HQVdNYTZ5cHk3R3Yyc01HWG1MaWZTb1hxc05kVmx6RVNRN1dKMQ0Kck9iazRNMTU1ZWVmK1BjN3VXdEpGR3o4Qm1YeUs2SGE1TEZvYWxER0NNaFczaXJUNVgxZ3hNeUp5YWk4VklnNQ0KZ0V2UFRwREdoa2xQSW5LQ3crVk9HOEFWMkxKWk5RaXpYZS8vYmNjMTVCY1NuUmJITHN0ZS85Ymw1Z25pelRCOQ0KTFZwVERjMENnWUFvaW05V2FYMjRsSjc1Ny9iakRzQmJMQUpsMENEeUFvY0REVXJVUFNrYXZkNEZDNklxdHpBQQ0KR245WEdpaXFNZHlibXNCdm5zT3Z4Ty8wTVhBMzBVck9sRnh3cnhvY3JNNjQ5RE5mS2JnYWljRU5hRnZ2VHF5bQ0KVG9VVDJFL2NXOWlTQUJ1c3RvbEN3STkrWHYwQ1NVbEVvM0pFRWFUQXQ1eXZ3VkFKa2J1OVpRPT0NCi0tLS0tRU5EIFJTQSBQUklWQVRFIEtFWS0tLS0tDQo='
        ssl_cert: 'LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tDQpNSUlEQmpDQ0FlNENDUUROV2JNdmJvQTcwREFOQmdrcWhraUc5dzBCQVFzRkFEQkZNUXN3Q1FZRFZRUUdFd0pCDQpWVEVUTUJFR0ExVUVDQXdLVTI5dFpTMVRkR0YwWlRFaE1COEdBMVVFQ2d3WVNXNTBaWEp1WlhRZ1YybGtaMmwwDQpjeUJRZEhrZ1RIUmtNQjRYRFRFMk1ESXlOVEE0TkRrMU0xb1hEVEUzTURJeU5EQTRORGsxTTFvd1JURUxNQWtHDQpBMVVFQmhNQ1FWVXhFekFSQmdOVkJBZ01DbE52YldVdFUzUmhkR1V4SVRBZkJnTlZCQW9NR0VsdWRHVnlibVYwDQpJRmRwWkdkcGRITWdVSFI1SUV4MFpEQ0NBU0l3RFFZSktvWklodmNOQVFFQkJRQURnZ0VQQURDQ0FRb0NnZ0VCDQpBTkVTWUg1azFTYW42NnNTb25YK2YxK3FkcmhMdXE2S3dJOFhXYnBGWVNjcERSS1U2TmdmMkpTaFlpTjQrMHhHDQpVdVNsNDAwOGZQMnVDb1E1RU9OMVlOei9VM3ViRjBWa2QzQXQyQzI3TURrb2xhUnpaYTJTa1JCZmo5S2RrM0hnDQp0VkFRVys4VlVKb01BOXo0Yk5BTHR3c2YwQnJYZGVmWlRodGlLWHlQL055ZDRSWU1Xc1JpSzNwYWZodVpmb3FJDQo4ekxFY0tQN2g3Yk5aVFRzYmN3OWRaanE4STRXTU5tTjNONWlaN2JHbUd0YzZMbXF1eGQ2VWFIbE1WY0hFOUp2DQphckNLNnNRekRHU085dUY5Rmk4eTJGamxXWURMYnlsbHUvK2ZMZ2pwdWk4S0ZveUlTeHRORVNFcVRzemNxM0dIDQp0S3RPdW4xNzEvS0laVGlocWJkdzlCa0NBd0VBQVRBTkJna3Foa2lHOXcwQkFRc0ZBQU9DQVFFQXd4aGYyM3hnDQpNakQ0bC9BczVFWlQ1bDRoNUd5eWplb3k4anFFR2o1MmVZMjkwck0xdHo1S3ZydFJTdW1FakMzZVhDZjF0bS80DQpUTTRaRXNhcmtuUmw4OTI1MVM2VmZFczFtbkRCSi9vcnRVT1ZOVE5hQ3lMTFhVZHZSKy82bTZlaXhISW12YlRRDQppdmYxQ21UcWhhcWpvZ3FxempNb2FlSlllbU9XYzN6cFNZN3N3dC9hcnZrMDhLMkdrL3UvS09MSm9kM21ac0ltDQpUN01aL3FsMDNhYW9rKzNnQ2NuYSt3S3NkQUtrUGlkNWo4YnUxMHpzdHk1UXNYSUxIOHFhV1BZdXZTeUdFUEJlDQo0UDB0ZXY5dUR6RjdvVkJVTUkyQ3JFeC9udVRJcEM5UWlCYjVyODM0WnRnM2xBREt4cWY0RXJZV1B1MjhJQmI5DQo5TUxlNW1pS2pzYXlHQT09DQotLS0tLUVORCBDRVJUSUZJQ0FURS0tLS0t'
        cache_dir: '/var/spool/squid'
        log_file: '/var/log/squid/access.log'
        enable_https: true
      series: 'centos7'
