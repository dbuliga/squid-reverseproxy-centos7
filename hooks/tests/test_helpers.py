import os
import json
import yaml

from os.path import dirname

from testtools import TestCase
from testtools.matchers import AfterPreprocessing, Contains, Not
from mock import patch

import hooks
from charmhelpers.core.hookenv import Serializable


def normalize_whitespace(data):
    return ' '.join(chunk for chunk in data.split())


@patch.dict(os.environ, {"CHARM_DIR": dirname(dirname(dirname(__file__)))})
class SquidConfigTest(TestCase):

    def _assert_contents(self, *expected):
        def side_effect(got):
            for chunk in expected:
                self.assertThat(got, AfterPreprocessing(
                    normalize_whitespace,
                    Contains(normalize_whitespace(chunk))))
        return side_effect

    def _assert_not_contents(self, *expected):
        def side_effect(got):
            for chunk in expected:
                self.assertThat(got, AfterPreprocessing(
                    normalize_whitespace,
                    Not(Contains(normalize_whitespace(chunk)))))
        return side_effect

    def _apply_patch(self, name):
        p = patch(name)
        mocked_name = p.start()
        self.addCleanup(p.stop)
        return mocked_name

    def setUp(self):
        super(SquidConfigTest, self).setUp()
        self.get_reverse_sites = self._apply_patch('hooks.get_reverse_sites')
        self.get_forward_sites = self._apply_patch('hooks.get_forward_sites')
        self.config_get = self._apply_patch('hooks.config_get')
        self.write_squid3_config = self._apply_patch(
            'hooks.write_squid3_config')
        self.apt_pkg = self._apply_patch('hooks.apt_pkg')

        self.apt_pkg.version_compare.return_value = -1
        self.get_forward_sites.return_value = {}

    def test_squid_config_no_sites(self):
        self.config_get.return_value = Serializable({
            "refresh_patterns": "",
            "cache_size_mb": 1024,
            "cache_mem_mb": 256,
            "target_objs_per_dir": 1024,
            "avg_obj_size_kb": 1024,
            "via": "on",
        })
        self.get_reverse_sites.return_value = None
        self.write_squid3_config.side_effect = self._assert_contents(
            """
            always_direct deny all
            """,
            """
            via on
            """,
            )
        hooks.construct_squid3_config()

    def test_squid_config_via_off(self):
        self.config_get.return_value = Serializable({
            "refresh_patterns": "",
            "cache_size_mb": 1024,
            "cache_mem_mb": 256,
            "target_objs_per_dir": 1024,
            "avg_obj_size_kb": 1024,
            "via": "off",
        })
        self.get_reverse_sites.return_value = None
        self.write_squid3_config.side_effect = self._assert_contents(
            """
            via off
            """,
            )
        hooks.construct_squid3_config()

    def test_squid_config_refresh_pattern_json(self):
        self.config_get.return_value = Serializable({
            "refresh_patterns": json.dumps(
                {"http://www.ubuntu.com":
                 {"min": 0, "percent": 20, "max": 60}}),
            "cache_size_mb": 1024,
            "cache_mem_mb": 256,
            "target_objs_per_dir": 1024,
            "avg_obj_size_kb": 1024,
        })
        self.get_reverse_sites.return_value = None
        self.write_squid3_config.side_effect = self._assert_contents(
            """
            refresh_pattern http://www.ubuntu.com 0 20% 60
            """,
            )
        hooks.construct_squid3_config()

    def test_squid_config_refresh_pattern_yaml(self):
        self.config_get.return_value = Serializable({
            "refresh_patterns": yaml.dump(
                {"http://www.ubuntu.com":
                 {"min": 0, "percent": 20, "max": 60}}),
            "cache_size_mb": 1024,
            "cache_mem_mb": 256,
            "target_objs_per_dir": 1024,
            "avg_obj_size_kb": 1024,
        })
        self.get_reverse_sites.return_value = None
        self.write_squid3_config.side_effect = self._assert_contents(
            """
            refresh_pattern http://www.ubuntu.com 0 20% 60
            """,
            )
        hooks.construct_squid3_config()

    def test_squid_config_refresh_pattern_options(self):
        self.config_get.return_value = Serializable({
            "refresh_patterns": yaml.dump(
                {"http://www.ubuntu.com":
                 {"min": 0, "percent": 20, "max": 60,
                  "options": ["override-lastmod",
                              "reload-into-ims"]}}),
            "cache_size_mb": 1024,
            "cache_mem_mb": 256,
            "target_objs_per_dir": 1024,
            "avg_obj_size_kb": 1024,
        })
        self.get_reverse_sites.return_value = None
        self.write_squid3_config.side_effect = self._assert_contents(
            """
            refresh_pattern http://www.ubuntu.com 0 20% 60
            override-lastmod reload-into-ims
            refresh_pattern . 30 20% 4320
            """,
            )
        hooks.construct_squid3_config()

    def test_squid_config_refresh_pattern_default(self):
        self.config_get.return_value = Serializable({
            "refresh_patterns": yaml.dump(
                {".":
                 {"min": 0, "percent": 20, "max": 60,
                  "options": ["override-lastmod",
                              "reload-into-ims"]}}),
            "cache_size_mb": 1024,
            "cache_mem_mb": 256,
            "target_objs_per_dir": 1024,
            "avg_obj_size_kb": 1024,
        })
        self.get_reverse_sites.return_value = None
        self.write_squid3_config.side_effect = self._assert_contents(
            """
            refresh_pattern . 0 20% 60
            override-lastmod reload-into-ims
            """,
            )
        hooks.construct_squid3_config()

    def test_squid_config_no_sitenames(self):
        self.config_get.return_value = Serializable({
            "refresh_patterns": "",
            "cache_size_mb": 1024,
            "cache_mem_mb": 256,
            "target_objs_per_dir": 1024,
            "avg_obj_size_kb": 1024,
        })
        self.get_reverse_sites.return_value = {
            None: [
                hooks.Server("website_1__foo_1", "1.2.3.4", 4242, ''),
                hooks.Server("website_1__foo_2", "1.2.3.5", 4242, ''),
                ],
            }
        self.write_squid3_config.side_effect = self._assert_contents(
            """
            acl no_sitename_acl myport
            http_access allow accel_ports no_sitename_acl
            never_direct allow no_sitename_acl
            """,
            """
            cache_peer 1.2.3.4 parent 4242 0 name=website_1__foo_1 no-query
            no-digest originserver round-robin login=PASS
            cache_peer_access website_1__foo_1 allow no_sitename_acl
            cache_peer_access website_1__foo_1 deny all
            """,
            """
            cache_peer 1.2.3.5 parent 4242 0 name=website_1__foo_2 no-query
            no-digest originserver round-robin login=PASS
            cache_peer_access website_1__foo_2 allow no_sitename_acl
            cache_peer_access website_1__foo_2 deny all
            """
            )
        hooks.construct_squid3_config()

    def test_squid_config_with_domain(self):
        self.config_get.return_value = Serializable({
            "refresh_patterns": "",
            "cache_size_mb": 1024,
            "cache_mem_mb": 256,
            "target_objs_per_dir": 1024,
            "avg_obj_size_kb": 1024,
        })
        self.get_reverse_sites.return_value = {
            "foo.com": [
                hooks.Server("foo_com__website_1__foo_1",
                             "1.2.3.4", 4242, "forceddomain=example.com"),
                hooks.Server("foo_com__website_1__foo_2",
                             "1.2.3.5", 4242, "forceddomain=example.com"),
                ],
            }
        self.write_squid3_config.side_effect = self._assert_contents(
            """
            acl s_1_acl dstdomain foo.com
            http_access allow accel_ports s_1_acl
            http_access allow CONNECT SSL_ports s_1_acl
            always_direct allow CONNECT SSL_ports s_1_acl
            always_direct deny s_1_acl
            """,
            """
            cache_peer 1.2.3.4 parent 4242 0 name=foo_com__website_1__foo_1
            no-query no-digest originserver round-robin login=PASS
            forceddomain=example.com
            cache_peer_access foo_com__website_1__foo_1 allow s_1_acl
            cache_peer_access foo_com__website_1__foo_1 deny all
            """,
            """
            cache_peer 1.2.3.5 parent 4242 0 name=foo_com__website_1__foo_2
            no-query no-digest originserver round-robin login=PASS
            forceddomain=example.com
            cache_peer_access foo_com__website_1__foo_2 allow s_1_acl
            cache_peer_access foo_com__website_1__foo_2 deny all
            """
            )
        hooks.construct_squid3_config()

    def test_with_domain_no_servers_only_direct(self):
        self.config_get.return_value = Serializable({
            "refresh_patterns": "",
            "cache_size_mb": 1024,
            "cache_mem_mb": 256,
            "target_objs_per_dir": 1024,
            "avg_obj_size_kb": 1024,
        })
        self.get_reverse_sites.return_value = {
            "foo.com": [
                ],
            }
        self.write_squid3_config.side_effect = self._assert_contents(
            """
            acl s_1_acl dstdomain foo.com
            http_access allow accel_ports s_1_acl
            http_access allow CONNECT SSL_ports s_1_acl
            always_direct allow s_1_acl
            """,
            )
        hooks.construct_squid3_config()

    def test_with_balancer_no_servers_only_direct(self):
        self.config_get.return_value = Serializable({
            "refresh_patterns": "",
            "cache_size_mb": 1024,
            "cache_mem_mb": 256,
            "target_objs_per_dir": 1024,
            "avg_obj_size_kb": 1024,
            "x_balancer_name_allowed": True,
        })
        self.get_reverse_sites.return_value = {
            "foo.com": [],
            }
        self.write_squid3_config.side_effect = self._assert_contents(
            """
            acl s_1_acl dstdomain foo.com
            http_access allow accel_ports s_1_acl
            http_access allow CONNECT SSL_ports s_1_acl
            always_direct allow s_1_acl
            """,
            """
            acl s_1_balancer req_header X-Balancer-Name foo\.com
            http_access allow accel_ports s_1_balancer
            http_access allow CONNECT SSL_ports s_1_balancer
            always_direct allow s_1_balancer
            """,
            )
        hooks.construct_squid3_config()

    def test_with_balancer_name(self):
        self.config_get.return_value = Serializable({
            "refresh_patterns": "",
            "cache_size_mb": 1024,
            "cache_mem_mb": 256,
            "target_objs_per_dir": 1024,
            "avg_obj_size_kb": 1024,
            "x_balancer_name_allowed": True,
            })
        self.get_reverse_sites.return_value = {
            "foo.com": [
                hooks.Server("foo_com__website_1__foo_1",
                             "1.2.3.4", 4242, ''),
                hooks.Server("foo_com__website_1__foo_2",
                             "1.2.3.5", 4242, ''),
                ],
            }
        self.write_squid3_config.side_effect = self._assert_contents(
            """
            acl s_1_acl dstdomain foo.com
            http_access allow accel_ports s_1_acl
            http_access allow CONNECT SSL_ports s_1_acl
            always_direct allow CONNECT SSL_ports s_1_acl
            always_direct deny s_1_acl
            """,
            """
            acl s_1_balancer req_header X-Balancer-Name foo\.com
            http_access allow accel_ports s_1_balancer
            http_access allow CONNECT SSL_ports s_1_balancer
            always_direct allow CONNECT SSL_ports s_1_balancer
            always_direct deny s_1_balancer
            """,
            """
            cache_peer 1.2.3.4 parent 4242 0 name=foo_com__website_1__foo_1
            no-query no-digest originserver round-robin login=PASS
            cache_peer_access foo_com__website_1__foo_1 allow s_1_acl
            cache_peer_access foo_com__website_1__foo_1 allow s_1_balancer
            cache_peer_access foo_com__website_1__foo_1 deny all
            """,
            """
            cache_peer 1.2.3.5 parent 4242 0 name=foo_com__website_1__foo_2
            no-query no-digest originserver round-robin login=PASS
            cache_peer_access foo_com__website_1__foo_2 allow s_1_acl
            """
            )
        hooks.construct_squid3_config()

    def test_forward_enabled(self):
        self.config_get.return_value = Serializable({
            "enable_forward_proxy": True,
            "refresh_patterns": "",
            "cache_size_mb": 1024,
            "cache_mem_mb": 256,
            "target_objs_per_dir": 1024,
            "avg_obj_size_kb": 1024,
        })
        self.get_reverse_sites.return_value = {
            "foo.com": [],
            }
        self.get_forward_sites.return_value = [
            {'private-address': '1.2.3.4', 'name': 'service_unit_0'},
            {'private-address': '2.3.4.5', 'name': 'service_unit_1'},
        ]
        self.write_squid3_config.side_effect = self._assert_contents(
            """
            acl fwd_service_unit_0 src 1.2.3.4
            http_access allow fwd_service_unit_0
            http_access allow CONNECT SSL_ports fwd_service_unit_0
            always_direct allow fwd_service_unit_0
            always_direct allow CONNECT SSL_ports fwd_service_unit_0
            acl fwd_service_unit_1 src 2.3.4.5
            http_access allow fwd_service_unit_1
            http_access allow CONNECT SSL_ports fwd_service_unit_1
            always_direct allow fwd_service_unit_1
            always_direct allow CONNECT SSL_ports fwd_service_unit_1
            """,
            """
            acl s_1_acl dstdomain foo.com
            http_access allow accel_ports s_1_acl
            http_access allow CONNECT SSL_ports s_1_acl
            always_direct allow s_1_acl
            """
            )
        hooks.construct_squid3_config()

    def test_squid_config_cache_enabled(self):
        self.config_get.return_value = Serializable({
            "refresh_patterns": "",
            "cache_size_mb": 1024,
            "cache_mem_mb": 256,
            "cache_dir": "/var/run/squid3",
            "target_objs_per_dir": 16,
            "avg_obj_size_kb": 4,
        })
        self.get_reverse_sites.return_value = None
        self.write_squid3_config.side_effect = self._assert_contents(
            """
            cache_dir aufs /var/run/squid3 1024 32 512
            cache_mem 256 MB
            """,
            )
        hooks.construct_squid3_config()

    def test_squid_config_disk_cache_disabled(self):
        self.config_get.return_value = Serializable({
            "refresh_patterns": "",
            "cache_size_mb": 0,
            "cache_mem_mb": 256,
            "cache_dir": "/var/run/squid3",
            "target_objs_per_dir": 16,
            "avg_obj_size_kb": 4,
        })
        self.get_reverse_sites.return_value = None
        self.write_squid3_config.side_effect = self._assert_not_contents(
            """
            cache_dir aufs
            """,
            )
        hooks.construct_squid3_config()

    def test_squid_config_cache_disabled(self):
        self.config_get.return_value = Serializable({
            "refresh_patterns": "",
            "cache_size_mb": 0,
            "cache_mem_mb": 0,
            "target_objs_per_dir": 1024,
            "avg_obj_size_kb": 1024,
        })
        self.get_reverse_sites.return_value = None
        self.write_squid3_config.side_effect = self._assert_contents(
            """
            cache deny all
            """,
            )
        hooks.construct_squid3_config()

        self.write_squid3_config.side_effect = self._assert_not_contents(
            """
            cache_mem
            """,
            )
        hooks.construct_squid3_config()

    def test_no_https(self):
        self.config_get.return_value = Serializable({
            "port": 3128,
            "port_options": "accel vhost",
            "enable_https": False,
            "refresh_patterns": "",
            "cache_size_mb": 0,
            "cache_mem_mb": 0,
            "target_objs_per_dir": 1024,
            "avg_obj_size_kb": 1024,
        })
        self.get_reverse_sites.return_value = None
        self.write_squid3_config.side_effect = self._assert_contents(
            """
            http_port 3128 accel vhost
            """,
            )
        hooks.construct_squid3_config()

        self.write_squid3_config.side_effect = self._assert_not_contents(
            """
            https_port
            """,
            )
        hooks.construct_squid3_config()

    def test_with_https(self):
        self.config_get.return_value = Serializable({
            "enable_https": True,
            "https_port": 443,
            "https_options": "accel vhost",
            "ssl_certfile": "/path/to/cert",
            "ssl_keyfile": "/path/to/key",
            "refresh_patterns": "",
            "cache_size_mb": 0,
            "cache_mem_mb": 0,
            "target_objs_per_dir": 1024,
            "avg_obj_size_kb": 1024,
        })
        self.get_reverse_sites.return_value = None
        self.write_squid3_config.side_effect = self._assert_contents(
            """
            https_port 443 accel vhost cert=/path/to/cert key=/path/to/key
            """,
            )
        hooks.construct_squid3_config()


class HelpersTest(TestCase):
    def test_gets_config(self):
        json_string = '{"foo": "BAR"}'
        with patch('subprocess.check_output') as check_output:
            check_output.return_value = json_string

            result = hooks.config_get()

            self.assertEqual(result['foo'], 'BAR')
            check_output.assert_called_with(['config-get', '--format=json'])

    def test_gets_config_with_scope(self):
        json_string = '{"foo": "BAR"}'
        with patch('subprocess.check_output') as check_output:
            check_output.return_value = json_string

            result = hooks.config_get(scope='baz')

            self.assertEqual(result['foo'], 'BAR')
            check_output.assert_called_with(['config-get', 'baz',
                                             '--format=json'])
