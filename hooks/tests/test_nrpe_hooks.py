import os
import grp
import pwd
import subprocess

from testtools import TestCase
from mock import patch, call

import hooks

from charmhelpers.contrib.charmsupport import nrpe
from charmhelpers.core.hookenv import Serializable


class NRPERelationTest(TestCase):
    """Tests for the update_nrpe_checks hook.

    Half of this is already tested in the tests for charmsupport.nrpe, but
    as the hook in the charm pre-dates that, the tests are left here to ensure
    backwards-compatibility.

    """
    patches = {
        'config': {'object': nrpe},
        'config_get': {'object': hooks},
        'log': {'object': nrpe},
        'getpwnam': {'object': pwd},
        'getgrnam': {'object': grp},
        'mkdir': {'object': os},
        'chown': {'object': os},
        'exists': {'object': os.path},
        'listdir': {'object': os},
        'remove': {'object': os},
        'open': {'object': nrpe, 'create': True},
        'isfile': {'object': os.path},
        'call': {'object': subprocess},
        'install_nrpe_scripts': {'object': hooks},
        'relation_ids': {'object': nrpe},
        'relation_set': {'object': nrpe},
    }

    def setUp(self):
        super(NRPERelationTest, self).setUp()
        self.patched = {}
        # Mock the universe.
        for attr, data in self.patches.items():
            create = data.get('create', False)
            patcher = patch.object(data['object'], attr, create=create)
            self.patched[attr] = patcher.start()
            self.addCleanup(patcher.stop)
        if 'JUJU_UNIT_NAME' not in os.environ:
            os.environ['JUJU_UNIT_NAME'] = 'test'

    def check_call_counts(self, **kwargs):
        for attr, expected in kwargs.items():
            patcher = self.patched[attr]
            self.assertEqual(expected, patcher.call_count, attr)

    def test_update_nrpe_no_nagios_bails(self):
        config = {
            'nagios_context': 'test',
            'port': 8080,
        }
        self.patched['config'].return_value = Serializable(config)
        self.patched['config_get'].return_value = Serializable(config)
        self.patched['getpwnam'].side_effect = KeyError

        self.assertEqual(None, hooks.update_nrpe_checks())

        expected = 'Nagios user not set up, nrpe checks not updated'
        self.patched['log'].assert_called_once_with(expected)
        self.check_call_counts(log=1, config=1, getpwnam=1)

    def test_update_nrpe_removes_existing_config(self):
        config = {
            'nagios_context': 'test',
            'nagios_check_http_params': '-u http://example.com/url',
            'port': 8080,
        }
        self.patched['config'].return_value = Serializable(config)
        self.patched['config_get'].return_value = Serializable(config)
        self.patched['exists'].return_value = True
        self.patched['listdir'].return_value = [
            'foo', 'bar.cfg', 'check_squidrp.cfg']

        self.assertEqual(None, hooks.update_nrpe_checks())

        expected = '/var/lib/nagios/export/check_squidrp.cfg'
        self.patched['remove'].assert_called_once_with(expected)
        self.check_call_counts(config=1, getpwnam=1, getgrnam=1,
                               exists=5, remove=1, open=4, listdir=2)

    def test_update_nrpe_uses_check_squidpeers(self):
        config = {
            'nagios_context': 'test',
            'port': '8080',
        }
        self.patched['config'].return_value = Serializable(config)
        self.patched['config_get'].return_value = Serializable(config)
        self.patched['exists'].return_value = True
        self.patched['isfile'].return_value = False

        self.assertEqual(None, hooks.update_nrpe_checks())
        self.assertEqual(2, self.patched['open'].call_count)
        filename = 'check_squidpeers.cfg'

        service_file_contents = """
#---------------------------------------------------
# This file is Juju managed
#---------------------------------------------------
define service {
    use                             active-service
    host_name                       test-test
    service_description             test-test[squidpeers] Check Squid Peers
    check_command                   check_nrpe!check_squidpeers
    servicegroups                   test
}
"""
        self.patched['open'].assert_has_calls(
            [call('/etc/nagios/nrpe.d/%s' % filename, 'w'),
             call('/var/lib/nagios/export/service__test-test_%s' %
                  filename, 'w'),
             call().__enter__().write(service_file_contents),
             call().__enter__().write('# check squidpeers\n'),
             call().__enter__().write(
                 'command[check_squidpeers]='
                 '/check_squidpeers -p 8080\n')],
            any_order=True)

        self.check_call_counts(config=1, getpwnam=1, getgrnam=1,
                               exists=3, open=2, listdir=1)

    def test_update_nrpe_with_check_url(self):
        config = {
            'nagios_context': 'test',
            'nagios_check_http_params': '-u foo -H bar',
            'port': '8080',
        }
        self.patched['config'].return_value = Serializable(config)
        self.patched['config_get'].return_value = Serializable(config)
        self.patched['exists'].return_value = True
        self.patched['isfile'].return_value = False

        self.assertEqual(None, hooks.update_nrpe_checks())
        self.assertEqual(4, self.patched['open'].call_count)
        filename = 'check_squidrp.cfg'

        service_file_contents = """
#---------------------------------------------------
# This file is Juju managed
#---------------------------------------------------
define service {
    use                             active-service
    host_name                       test-test
    service_description             test-test[squidrp] Check Squid
    check_command                   check_nrpe!check_squidrp
    servicegroups                   test
}
"""
        self.patched['open'].assert_has_calls(
            [call('/etc/nagios/nrpe.d/%s' % filename, 'w'),
             call('/var/lib/nagios/export/service__test-test_%s' %
                  filename, 'w'),
             call().__enter__().write(service_file_contents),
             call().__enter__().write('# check squidrp\n'),
             call().__enter__().write(
                 'command[check_squidrp]=/check_http -u foo -H bar\n')],
            any_order=True)

        self.check_call_counts(config=1, getpwnam=1, getgrnam=1,
                               exists=5, open=4, listdir=2)

    def test_update_nrpe_with_no_check_path(self):
        config = {
            'nagios_context': 'test',
            'services': '- {service_name: i_ytimg_com}',
            'port': '8080',
        }
        self.patched['config'].return_value = Serializable(config)
        self.patched['config_get'].return_value = Serializable(config)
        self.patched['exists'].return_value = True

        self.assertEqual(None, hooks.update_nrpe_checks())

        self.check_call_counts(config=1, getpwnam=1, getgrnam=1,
                               exists=3, open=2, listdir=1)

    def test_update_nrpe_with_services_and_host_header(self):
        config = {
            'nagios_context': 'test',
            'services': '- {service_name: i_ytimg_com, nrpe_check_path: /}',
            'port': '8080',
        }
        self.patched['config'].return_value = Serializable(config)
        self.patched['config_get'].return_value = Serializable(config)
        self.patched['exists'].return_value = True

        self.assertEqual(None, hooks.update_nrpe_checks())

        self.check_call_counts(config=1, getpwnam=1, getgrnam=1,
                               exists=5, open=4, listdir=2)
        expected = ('command[check_squid-i_ytimg_com]=/check_http '
                    '-I 127.0.0.1 -p 3128 --method=HEAD '
                    '-u http://i_ytimg_com/\n')
        self.patched['open'].assert_has_calls(
            [call('/etc/nagios/nrpe.d/check_squid-i_ytimg_com.cfg', 'w'),
             call().__enter__().write(expected)],
            any_order=True)

    def test_update_nrpe_with_dotted_service_name_and_host_header(self):
        config = {
            'nagios_context': 'test',
            'services': '- {service_name: i.ytimg.com, nrpe_check_path: /}',
            'port': '8080',
        }
        self.patched['config'].return_value = Serializable(config)
        self.patched['config_get'].return_value = Serializable(config)
        self.patched['exists'].return_value = True

        self.assertEqual(None, hooks.update_nrpe_checks())

        self.check_call_counts(config=1, getpwnam=1, getgrnam=1,
                               exists=5, open=4, listdir=2)
        expected = ('command[check_squid-i_ytimg_com]=/check_http '
                    '-I 127.0.0.1 -p 3128 --method=HEAD '
                    '-u http://i.ytimg.com/\n')
        self.patched['open'].assert_has_calls(
            [call('/etc/nagios/nrpe.d/check_squid-i_ytimg_com.cfg', 'w'),
             call().__enter__().write(expected)],
            any_order=True)

    def test_update_nrpe_with_services_and_balancer_name_header(self):
        config = {
            'nagios_context': 'test',
            'x_balancer_name_allowed': True,
            'services': '- {service_name: i_ytimg_com, nrpe_check_path: /}',
            'port': '8080',
        }
        self.patched['config'].return_value = Serializable(config)
        self.patched['config_get'].return_value = Serializable(config)
        self.patched['exists'].return_value = True

        self.assertEqual(None, hooks.update_nrpe_checks())

        self.check_call_counts(config=1, getpwnam=1, getgrnam=1,
                               exists=5, open=4, listdir=2)

        expected = ('command[check_squid-i_ytimg_com]=/check_http '
                    '-I 127.0.0.1 -p 3128 --method=HEAD -u http://localhost/ '
                    "-k 'X-Balancer-Name: i_ytimg_com'\n")
        self.patched['open'].assert_has_calls(
            [call('/etc/nagios/nrpe.d/check_squid-i_ytimg_com.cfg', 'w'),
             call().__enter__().write(expected)],
            any_order=True)

    def test_update_nrpe_with_services_and_optional_path(self):
        services = '- {nrpe_check_path: /foo.jpg, service_name: foo_com}\n'
        config = {
            'nagios_context': 'test',
            'services': services,
            'port': '8080',
        }
        self.patched['config'].return_value = Serializable(config)
        self.patched['config_get'].return_value = Serializable(config)
        self.patched['exists'].return_value = True

        self.assertEqual(None, hooks.update_nrpe_checks())

        self.check_call_counts(config=1, getpwnam=1, getgrnam=1,
                               exists=5, open=4, listdir=2)
        expected = ('command[check_squid-foo_com]=/check_http '
                    '-I 127.0.0.1 -p 3128 --method=HEAD '
                    '-u http://foo_com/foo.jpg\n')
        self.patched['open'].assert_has_calls(
            [call('/etc/nagios/nrpe.d/check_squid-foo_com.cfg', 'w'),
             call().__enter__().write(expected)],
            any_order=True)

    def test_update_nrpe_with_services_GET_if_query_string_in_path(self):
        services = ('- {nrpe_check_path: "/?path=foo.jpg", '
                    'service_name: foo_com}\n')
        config = {
            'nagios_context': 'test',
            'services': services,
            'port': '8080',
        }
        self.patched['config'].return_value = Serializable(config)
        self.patched['config_get'].return_value = Serializable(config)
        self.patched['exists'].return_value = True

        self.assertEqual(None, hooks.update_nrpe_checks())

        self.check_call_counts(config=1, getpwnam=1, getgrnam=1,
                               exists=5, open=4, listdir=2)
        expected = ('command[check_squid-foo_com]=/check_http '
                    '-I 127.0.0.1 -p 3128 --method=GET '
                    '-u http://foo_com/?path=foo.jpg\n')
        self.patched['open'].assert_has_calls(
            [call('/etc/nagios/nrpe.d/check_squid-foo_com.cfg', 'w'),
             call().__enter__().write(expected)],
            any_order=True)

    def test_update_nrpe_restarts_service(self):
        config = {
            'nagios_context': 'test',
            'nagios_check_http_params': '-u foo -p 3128',
            'port': '8080',
        }
        self.patched['config'].return_value = Serializable(config)
        self.patched['config_get'].return_value = Serializable(config)
        self.patched['exists'].return_value = True

        self.assertEqual(None, hooks.update_nrpe_checks())

        expected = ['service', 'nagios-nrpe-server', 'restart']
        self.assertEqual(expected, self.patched['call'].call_args[0][0])
        self.check_call_counts(config=1, getpwnam=1, getgrnam=1,
                               exists=5, open=4, listdir=2, call=1)
