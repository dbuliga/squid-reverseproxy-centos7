import subprocess
import apt_pkg
import re


default_squid3_config_dir = "/etc/squid3"
default_squid3_config = "%s/squid.conf" % default_squid3_config_dir
default_squid3_config_cache_dir = "/var/run/squid3"
PACKAGES = ['squid3', 'squidclient', 'python-jinja2', 'snmp']
squid = 'squid3'
default_squid3_file = '/files/default.squid3'
SERVICE_PATH = '/etc/default/squid3'
service_affecting_packages = ['squid3']
DEFAULT = {
    'ssl_keyfile': '/etc/squid3/ssl/cert.key',
    'ssl_certfile': '/etc/squid3/ssl/cert.crt',
    'log_file': '/var/log/squid3/access.log',
    'cache_dir': '/var/spool/squid3'
}


def ensure_package_status(packages, status):
    if status in ['install', 'hold']:
        selections = ''.join(['{} {}\n'.format(package, status)
                              for package in packages])
        dpkg = subprocess.Popen(['dpkg', '--set-selections'],
                                stdin=subprocess.PIPE)
        dpkg.communicate(input=selections)


def is_local_acl_defs_required():
    apt_pkg.init()
    cache = apt_pkg.Cache()
    pkg = cache['squid3']
    # From 3.2 the manager, localhost, and to_localhost ACL definitions are
    # now built-in. So, detect the package version and remove the
    # definitions from the generated config
    need_localacl_defs = apt_pkg.version_compare(pkg.current_ver.ver_str,
                                                 '3.2')
    return need_localacl_defs


def service_squid3(action=None, squid3_config=default_squid3_config):
    """Convenience function to start/stop/restart/reload the squid3
    service"""
    if action is None or squid3_config is None:
        return
    elif action == "check":
        check_cmd = ['/usr/sbin/squid3', '-f', squid3_config, '-k', 'parse']
        return not subprocess.call(check_cmd)
    elif action == 'status':
        status = subprocess.check_output(['status', 'squid3'])
        return re.search('running', status) is not None
    elif action in ('start', 'stop', 'reload', 'restart'):
        return not subprocess.call([action, 'squid3'])
