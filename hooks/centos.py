import subprocess
from charmhelpers.core.host import cmp_pkgrevno

default_squid3_config_dir = "/etc/squid"
default_squid3_config = "%s/squid.conf" % default_squid3_config_dir
default_squid3_config_cache_dir = "/var/run/squid"
default_squid3_file = '/files/default.squid'
SERVICE_PATH = "/usr/lib/systemd/system/squid.service"
PACKAGES = ['squid', 'python-jinja2', 'net-snmp']
service_affecting_packages = ['squid']
DEFAULT = {
    'ssl_keyfile': '/etc/squid/ssl/cert.key',
    'ssl_certfile': '/etc/squid/ssl/cert.crt',
    'log_file': '/var/log/squid/access.log',
    'cache_dir': '/var/spool/squid'
}


def ensure_package_status(packages, status):
    pass


def is_local_acl_defs_required():
    # From 3.2 the manager, localhost, and to_localhost ACL definitions are now
    # built-in. So, detect the package version and remove the definitions from
    # the generated config
    need_localacl_defs = cmp_pkgrevno('squid', '3.2')
    return need_localacl_defs


def service_squid3(action=None, squid3_config=default_squid3_config):
    """Convenience function to start/stop/restart/reload the squid
    service"""

    if action is None or squid3_config is None:
        return
    elif action == "check":
        check_cmd = ['/usr/sbin/squid', '-f', squid3_config, '-k', 'parse']
        return not subprocess.call(check_cmd)
    elif action == 'status':
        return_code = subprocess.call(['service', 'squid', 'status'])
        return return_code == 0
    elif action in ('start', 'stop', 'reload', 'restart'):
        return not subprocess.call(['service', 'squid', action])
