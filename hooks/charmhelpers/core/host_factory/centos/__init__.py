import subprocess
import yum

from .. import HostBase


class Host(HostBase):
    '''
    Implementation of HostBase for CentOS
    '''

    def _add_group(self, group_name, system_group=False):
        cmd = ['groupadd']
        if system_group:
            cmd.append('-r')
        cmd.append(group_name)
        subprocess.check_call(cmd)

    def _lsb_release(self):
        """Return /etc/os-release in a dict"""
        d = {}
        with open('/etc/os-release', 'r') as lsb:
            for l in lsb:
                if len(l.split('=')) != 2:
                    continue
                k, v = l.split('=')
                d[k.strip()] = v.strip()
        return d

    def _cmp_pkgrevno(self, package, revno, pkgcache=None):
        """Compare supplied revno with the revno of the installed package

        *  1 => Installed revno is greater than supplied arg
        *  0 => Installed revno is the same as supplied arg
        * -1 => Installed revno is less than supplied arg

        This function imports apt_cache function from charmhelpers.fetch if
        the pkgcache argument is None. Be sure to add charmhelpers.fetch if
        you call this function, or pass an apt_pkg.Cache() instance.
        """
        if not pkgcache:
            y = yum.YumBase()
            packages = y.doPackageLists()
            pck = {}
            for i in packages["installed"]:
                pck[i.Name] = i.version
                pkgcache = pck
        pkg = pkgcache[package]
        if pkg > revno:
            return 1
        if pkg < revno:
            return -1
        return 0
