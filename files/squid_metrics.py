#!/bin/env python
import shlex
import sys
import socket
import subprocess
import ConfigParser


METRIC_TYPES = {
    'cacheSysVMsize': 'g',  # Storage Mem size in KB
    'cacheSysStorage': 'g',  # Storage Swap size in KB
    'cacheUptime': 'c',  # The Uptime of the cache in timeticks
    'cacheMemMaxSize': 'g',  # The value of the cache_mem parameter in MB
    'cacheSwapMaxSize': 'g',  # The total of the cache_dir space allocated in MB
    'cacheSwapHighWM': 'g',  # Cache Swap High Water Mark
    'cacheSwapLowWM': 'g',  # Cache Swap Low Water Mark
    'cacheUniqName': 'g',  # Cache unique host name
    'cacheSysPageFaults': 'c',  # Page faults with physical i/o
    'cacheSysNumReads': 'c',  # HTTP I/O number of reads
    'cacheMemUsage': 'g',  # Total memory accounted for KB
    'cacheCpuTime': 'c',  # Amount of cpu seconds consumed
    'cacheCpuUsage': 'g',  # The percentage use of the CPU
    'cacheMaxResSize': 'g',  # Maximum Resident Size in KB
    'cacheNumObjCount': 'g',  # Number of objects stored by the cache
    'cacheCurrentLRUExpiration': 'g',  # Storage LRU Expiration Age
    'cacheCurrentUnlinkRequests': 'g',  # Requests given to unlinkd
    'cacheCurrentUnusedFDescrCnt': 'g',  # Available number of file descriptors
    'cacheCurrentResFileDescrCnt': 'g',  # Reserved number of file descriptors
    'cacheCurrentFileDescrCnt': 'g',  # Number of file descriptors in use
    'cacheCurrentFileDescrMax': 'g',  # Highest file descriptors in use
    'cacheProtoClientHttpRequests': 'c',  # Number of HTTP requests received
    'cacheHttpHits': 'c',  # Number of HTTP Hits sent to clients from cache
    'cacheHttpErrors': 'c',  # Number of HTTP Errors sent to clients
    'cacheHttpInKb': 'c',  # Number of HTTP KB's received from clients
    'cacheHttpOutKb': 'c',  # Number of HTTP KB's sent to clients
    'cacheIcpPktsSent': 'c',  # Number of ICP messages sent
    'cacheIcpPktsRecv': 'c',  # Number of ICP messages received
    'cacheIcpKbSent': 'c',  # Number of ICP KB's transmitted
    'cacheIcpKbRecv': 'c',  # Number of ICP KB's received
    'cacheServerRequests': 'c',  # All requests from the client for the cache server
    'cacheServerErrors': 'c',  # All errors for the cache server from client requests
    'cacheServerInKb': 'c',  # KB's of traffic received from servers
    'cacheServerOutKb': 'c',  # KB's of traffic sent to servers
    'cacheCurrentSwapSize': 'g',  # Storage Swap size
    'cacheClients': 'g',  # Number of clients accessing cache
    'cacheMedianTime.1': 'g',  # The value used to index the table 1/5/60
    'cacheMedianTime.5': 'g',  #
    'cacheMedianTime.60': 'g',  #
    'cacheHttpAllSvcTime.1': 'g',  # HTTP all service time
    'cacheHttpAllSvcTime.5': 'g',  #
    'cacheHttpAllSvcTime.60': 'g',  #
    'cacheHttpMissSvcTime.1': 'g',  # HTTP miss service time
    'cacheHttpMissSvcTime.5': 'g',  #
    'cacheHttpMissSvcTime.60': 'g',  #
    'cacheHttpNmSvcTime.1': 'g',  # HTTP hit not-modified service time
    'cacheHttpNmSvcTime.5': 'g',  #
    'cacheHttpNmSvcTime.60': 'g',  #
    'cacheHttpHitSvcTime.1': 'g',  # HTTP hit service time
    'cacheHttpHitSvcTime.5': 'g',  #
    'cacheHttpHitSvcTime.60': 'g',  #
    'cacheIcpQuerySvcTime.1': 'g',  # ICP query service time
    'cacheIcpQuerySvcTime.5': 'g',  #
    'cacheIcpQuerySvcTime.60': 'g',  #
    'cacheIcpReplySvcTime.1': 'g',  # ICP reply service time
    'cacheIcpReplySvcTime.5': 'g',  #
    'cacheIcpReplySvcTime.60': 'g',  #
    'cacheDnsSvcTime.1': 'g',  # DNS service time
    'cacheDnsSvcTime.5': 'g',  #
    'cacheDnsSvcTime.60': 'g',  #
    'cacheRequestHitRatio.1': 'g',  # Request Hit Ratios
    'cacheRequestHitRatio.5': 'g',  #
    'cacheRequestHitRatio.60': 'g',  #
    'cacheRequestByteRatio.1': 'g',  # Byte Hit Ratios
    'cacheRequestByteRatio.5': 'g',  #
    'cacheRequestByteRatio.60': 'g',  #
    'cacheHttpNhSvcTime.1': 'g',  # HTTP refresh hit service time
    'cacheHttpNhSvcTime.5': 'g',  #
    'cacheHttpNhSvcTime.60': 'g',  #
    'cacheIpEntries': 'g',  # IP Cache Entries
    'cacheIpRequests': 'c',  # Number of IP Cache requests
    'cacheIpHits': 'c',  # Number of IP Cache hits
    'cacheIpPendingHits': 'g',  # Number of IP Cache pending hits
    'cacheIpNegativeHits': 'c',  # Number of IP Cache pending hits
    'cacheIpMisses': 'c',  # Number of IP Cache misses
    'cacheBlockingGetHostByName': 'c',  # Number of blocking gethostbyname requests
    'cacheAttemptReleaseLckEntries': 'c',  # Number of attempts to release locked IP
    'cacheFqdnEntries': 'g',  # FQDN Cache entries
    'cacheFqdnRequests': 'c',  # Number of FQDN Cache requests
    'cacheFqdnHits': 'c',  # Number of FQDN Cache hits
    'cacheFqdnPendingHits': 'g',  # Number of FQDN Cache pending hits
    'cacheFqdnNegativeHits': 'c',  # Number of FQDN Cache negative hits
    'cacheFqdnMisses': 'c',  # Number of FQDN Cache misses
    'cacheBlockingGetHostByAddr': 'c',  # Number of blocking gethostbyaddr requests
    'cacheDnsRequests': 'c',  # Number of external dnsserver requests
    'cacheDnsReplies': 'c',  # Number of external dnsserver replies
    'cacheDnsNumberServers': 'c',  # Number of external dnsserver processes
    'cachePeerIndex': 'g',  # A unique value, greater than zero for each cache_peer instance in the managed system.
    'cachePeerPortHttp': 'g',  # The port the peer listens for HTTP requests
    'cachePeerPortIcp': 'g',  # The port the peer listens for ICP requests should be 0 if not configured to send ICP requests
    'cachePeerType': 'g',  # Peer Type
    'cachePeerState': 'g',  # The operational state of this peer
    'cachePeerPingsSent': 'c',  # Number of pings sent to peer
    'cachePeerPingsAcked': 'c',  # Number of pings received from peer
    'cachePeerFetches': 'c',  # Number of times this peer was selected
    'cachePeerRtt': 'g',  # Last known round-trip time to the peer (in ms)
    'cachePeerIgnored': 'c',  # How many times this peer was ignored
    'cachePeerKeepAlSent': 'c',  # Number of keepalives sent
    'cachePeerKeepAlRecv': 'c',  # Number of keepalives received
    'cacheClientHttpRequests': 'c',  # Number of HTTP requests received from client
    'cacheClientHttpKb': 'c',  # Amount of total HTTP traffic to this client
    'cacheClientHttpHits': 'c',  # Number of hits in response to this client's HTTP requests
    'cacheClientHTTPHitKb': 'c',  # Amount of HTTP hit traffic in KB
    'cacheClientIcpRequests': 'c',  # Number of ICP requests received from client
    'cacheClientIcpKb': 'c',  # Amount of total ICP traffic to this client (child)
    'cacheClientIcpHits': 'c',  # Number of hits in response to this client's ICP requests
    'cacheClientIcpHitKb': 'c',  # Amount of ICP hit traffic in KB
}


config_path = "/usr/local/etc/squid_metrics.ini"
cmd = "snmpwalk -v1 -Cc -Oq -c %s -m /usr/share/squid3/mib.txt localhost:%s"


def clean(s):
    """Make a string safe for statsd scheme"""
    return s.replace('.', '-').replace('/', '-')


def load_config(path):
    config = ConfigParser.SafeConfigParser()
    config.read(config_path)
    return {
        'unit': config.get('metrics', 'unit'),
        'scheme': config.get('metrics', 'scheme'),
        'snmp_port': config.get('metrics', 'snmp_port'),
        'statsd_hostport': config.get('metrics', 'statsd_hostport'),
        'community': config.get('metrics', 'community'),
        'metrics': config.get('metrics', 'metrics').strip().split(','),
    }


class SNMPMetricFailed(Exception):
    pass


def get_snmp_value(cmd, metric):
    try:
        output = subprocess.check_output(cmd + (metric,))
    except subprocess.CalledProcessError:
        raise SNMPMetricFailed()

    if 'End of MIB' in output:
        # could not find metric name in SNMP
        raise SNMPMetricFailed()

    return output.strip()


def get_metrics(config):

    scheme = config['scheme'].replace('$UNIT', clean(config['unit']))
    snmp_cmd = tuple(
        shlex.split(cmd % (config['community'], config['snmp_port'])))

    try:
        raw_peer_names = get_snmp_value(snmp_cmd, 'cachePeerName')
    except SNMPMetricFailed:
        # no peers configured
        peer_names = {}
    else:
        lines = (line for line in raw_peer_names.split('\n') if ' ' in line)
        peer_names = dict(line[25:].split(' ') for line in lines)

    for metric in config['metrics']:

        if metric not in METRIC_TYPES:
            # TODO log failure?
            continue

        # sadly, have to do one call per metric
        try:
            output = get_snmp_value(snmp_cmd, metric)
        except SNMPMetricFailed:
            # skip metric
            # TODO log failure to read metric?
            continue

        metric_type = METRIC_TYPES[metric]

        # output is like "SQUID-MIB::<key> <value>"
        for line in output.split('\n'):
            # ensure only what we asked, as snmpwalk can be overly generous
            if metric in line:
                # strip leading 'SQUID-MIB::'
                key, value = line[11:].split()
                # use peer hostname rather than index
                if 'cachePeer' in key:
                    key, index = key.split('.')
                    name = peer_names.get(index, index)
                    key = clean(key) + '.' + clean(name)
                else:
                    # avoid introducing uneeded statsd heirarchy
                    key = clean(key.replace('.0', ''))
                if '$METRIC' in scheme:
                    name = scheme.replace('$METRIC', key)
                else:
                    name = scheme + ".%s" % key
                yield (name, value, metric_type)


if __name__ == '__main__':

    config = load_config(config_path)

    def send(x):
        print x

    if len(sys.argv) > 1 and sys.argv[1] == "send":
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        host, port = config['statsd_hostport'].split(':')
        sock.connect((host, int(port)))
        def send(x):
            sock.send(x)

    for metric in get_metrics(config):
        send(("%s:%s|%s" % metric).encode('utf8'))
