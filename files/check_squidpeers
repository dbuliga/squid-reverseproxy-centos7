#!/usr/bin/python

import argparse
from operator import itemgetter
import re
import subprocess
import sys


parent_re = re.compile(r'^Parent\s+:\s*(?P<name>\S+)')
status_re = re.compile(r'^Status\s+:\s*(?P<status>\S+)')

STATUS_UP = 'Up'


def parse(output):
    state = 'header'
    peers = []
    for line in output.splitlines():
        if state == 'header':
            if not line.strip():
                state = 'header gap'
        elif state == 'header gap':
            if not line.strip():
                state = 'peer'
                peers.append(dict())
            elif line.strip() == "There are no neighbors installed.":
                return peers
            else:
                raise AssertionError('Expecting blank line after header?: {}'.format(line))
        elif state == 'peer':
            if not line.strip():
                peers.append(dict())
            else:
                match = parent_re.match(line)
                if match:
                    peers[-1]['name'] = match.group('name')
                match = status_re.match(line)
                if match:
                    peers[-1]['up'] = match.group('status') == STATUS_UP
                    peers[-1]['status'] = match.group('status')
    return peers


def get_status(peers):
    ok = all(map(itemgetter('up'), peers))
    if not peers:
        retcode = 1
        message = 'Squid has no configured peers.'
    elif ok:
        retcode = 0
        message = 'All peers are UP according to squid.'
    else:
        retcode = 2
        peer_info = ["{}: {}".format(p['name'], p['status']) for p in peers
                     if not p['up']]
        message = 'The following peers are not UP according to squid: {}'.format(
                ", ".join(peer_info))
    return retcode, message


def main():
    parser = argparse.ArgumentParser(description='check_squidpeers')
    parser.add_argument('-p', '--port', default=3128,
                        help='Port number squid listens on')
    args = parser.parse_args()
    proc = subprocess.Popen(['squidclient', '-p', str(args.port), 'mgr:server_list'],
            stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout, stderr = proc.communicate()
    if proc.returncode != 0:
        print("Error running squidclient: %s" % stderr)
        return 2
    peers = parse(stdout)
    retcode, message = get_status(peers)
    print(message)
    return retcode


if __name__ == '__main__':
    sys.exit(main())
