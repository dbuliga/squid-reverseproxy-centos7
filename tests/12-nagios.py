#!/usr/bin/python3
import amulet
import requests

seconds = 20000

d = amulet.Deployment(series='trusty')

d.add('squid-reverseproxy')
d.add('nagios')
d.add('nrpe-external-master')

d.relate('nagios:monitors', 'squid-reverseproxy:monitors')
d.relate('nrpe-external-master:general-info', 'squid-reverseproxy:juju-info')
d.relate('nrpe-external-master:monitors', 'nagios:monitors')

d.expose('nagios')

try:
    d.setup(timeout=seconds)
except amulet.helpers.TimeoutError:
    amulet.raise_status(amulet.SKIP, msg="Environment wasn't stood up in time")
except:
    raise


##
# Set relationship aliases
##
squid_unit = d.sentry.unit['squid/0']
nagios_unit = d.sentry.unit['nagios/0']


def test_hosts_being_monitored():
    nagpwd = nagios_unit.file_contents('/var/lib/juju/nagios.passwd').strip()
    host_url = ("http://%s/cgi-bin/nagios3/status.cgi?"
                "hostgroup=all&style=hostdetail")
    r = requests.get(host_url % nagios_unit.info['public-address'],
                     auth=('nagiosadmin', nagpwd))
    if not r.text.find('squid'):
        amulet.raise_status(amulet.ERROR,
                            msg='Nagios is not monitoring the' +
                            ' host it supposed to.')


test_hosts_being_monitored()
