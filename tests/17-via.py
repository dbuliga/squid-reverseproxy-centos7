#!/usr/bin/python3
import amulet
import requests

seconds = 20000

d = amulet.Deployment(series='trusty')

d.add('apache2')
d.add('squid-reverseproxy')

d.relate('apache2:website-cache', 'squid-reverseproxy:cached-website')

d.expose('apache2')
d.expose('squid-reverseproxy')

try:
    d.setup(timeout=seconds)
except amulet.helpers.TimeoutError:
    amulet.raise_status(amulet.SKIP, msg="Environment wasn't stood up in time")
except:
    raise


##
# Set relationship aliases
##
squid_unit = d.sentry.unit['squid-reverseproxy/0']
apache_unit = d.sentry.unit['apache2/0']


def test_via_header():
    url = 'http://%s/'
    squid_request = requests.get(url % squid_unit.info['public-address'])
    # check headers
    if 'Via' not in squid_request.headers:
        amulet.raise_status(amulet.FAIL,
                            msg="Environment wasn't stood up in time")

    # config via off
    d.configure('squid-reverseproxy', {
        'via': 'off'
    })
    d.sentry.wait()

    # check headers again
    if 'Via' in squid_request.headers:
        amulet.raise_status(amulet.FAIL,
                            msg="Environment wasn't stood up in time")


test_via_header()
