#!/usr/bin/python3
import amulet
import requests

seconds = 20000

d = amulet.Deployment(series='trusty')

d.add('apache2')
d.add('squid-reverseproxy')

d.relate('apache2:website-cache', 'squid-reverseproxy:cached-website')

d.expose('apache2')
d.expose('squid-reverseproxy')

try:
    d.setup(timeout=seconds)
except amulet.helpers.TimeoutError:
    amulet.raise_status(amulet.SKIP, msg="Environment wasn't stood up in time")
except:
    raise


##
# Set relationship aliases
##
squid_unit = d.sentry.unit['squid-reverseproxy/0']
apache_unit = d.sentry.unit['apache2/0']


def test_web_proxy():
    url = 'http://%s/'
    squid_request = requests.get(url % squid_unit.info['public-address'])
    apache_request = requests.get(url % apache_unit.info['public-address'])
    if not squid_request.ok or not apache_request.ok:
        amulet.raise_status(amulet.FAIL,
                            msg="Error connecting.")
    if squid_request.text != apache_request.text:
        amulet.raise_status(amulet.FAIL,
                            msg="Squid response different from apache.")


test_web_proxy()
