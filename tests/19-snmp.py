#!/usr/bin/python3
import subprocess

import amulet

seconds = 20000

d = amulet.Deployment(series='trusty')

d.add('apache2')
d.add('squid-reverseproxy')

d.configure('squid-reverseproxy', {
    'snmp_community': 'juju',
    'snmp_allowed_ips': '0.0.0.0/0'
})

d.relate('apache2:website-cache', 'squid-reverseproxy:cached-website')

d.expose('squid-reverseproxy')

try:
    d.setup(timeout=seconds)
except amulet.helpers.TimeoutError:
    amulet.raise_status(amulet.SKIP, msg="Environment wasn't stood up in time")
except:
    raise


##
# Set relationship aliases
##
squid_unit = d.sentry.unit['squid-reverseproxy/0']


def test_snmp():
    try:
        subprocess.check_output(
            "squidclient -h %s mgr:info" % squid_unit.info['public-address'])
    except subprocess.CalledProcessError:
        amulet.raise_status(amulet.FAIL,
                            msg="Wasn't able to connect via SNMP.")


test_snmp()
